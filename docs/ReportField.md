# ReportField

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Protocol** | **string** | The protocol the address relates to | 
**Address** | **string** | The wallet/account the transaction occurred | 
**Currency** | **string** | The currency symbol | 
**EventId** | **string** | The ID of the event within a transaction | 
**Block** | **int64** | The block number the transaction occurred on | 
**Timestamp** | **int32** | The unix timestamp when the transaction was added to a block | 
**Hash** | **string** | The transaction ID | 
**Action** | **string** | The action type e.g. Transfer, Deposit, Staking Reward etc.. | 
**Value** | **string** | The amount of currency involved in the transaction (smallest unit) | 
**SenderAddress** | **string** | The address where the funds originated | 
**Fee** | **string** | How much was charged as a fee for processing the transaction | 
**Decimals** | **int32** | The number of decimals in one coin, used to convert smallest unit to 1 whole coin if needed | 
**Meta** | Pointer to [**NullableReportFieldMeta**](ReportFieldMeta.md) |  | [optional] 

## Methods

### NewReportField

`func NewReportField(protocol string, address string, currency string, eventId string, block int64, timestamp int32, hash string, action string, value string, senderAddress string, fee string, decimals int32, ) *ReportField`

NewReportField instantiates a new ReportField object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewReportFieldWithDefaults

`func NewReportFieldWithDefaults() *ReportField`

NewReportFieldWithDefaults instantiates a new ReportField object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetProtocol

`func (o *ReportField) GetProtocol() string`

GetProtocol returns the Protocol field if non-nil, zero value otherwise.

### GetProtocolOk

`func (o *ReportField) GetProtocolOk() (*string, bool)`

GetProtocolOk returns a tuple with the Protocol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetProtocol

`func (o *ReportField) SetProtocol(v string)`

SetProtocol sets Protocol field to given value.


### GetAddress

`func (o *ReportField) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *ReportField) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *ReportField) SetAddress(v string)`

SetAddress sets Address field to given value.


### GetCurrency

`func (o *ReportField) GetCurrency() string`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *ReportField) GetCurrencyOk() (*string, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *ReportField) SetCurrency(v string)`

SetCurrency sets Currency field to given value.


### GetEventId

`func (o *ReportField) GetEventId() string`

GetEventId returns the EventId field if non-nil, zero value otherwise.

### GetEventIdOk

`func (o *ReportField) GetEventIdOk() (*string, bool)`

GetEventIdOk returns a tuple with the EventId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventId

`func (o *ReportField) SetEventId(v string)`

SetEventId sets EventId field to given value.


### GetBlock

`func (o *ReportField) GetBlock() int64`

GetBlock returns the Block field if non-nil, zero value otherwise.

### GetBlockOk

`func (o *ReportField) GetBlockOk() (*int64, bool)`

GetBlockOk returns a tuple with the Block field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlock

`func (o *ReportField) SetBlock(v int64)`

SetBlock sets Block field to given value.


### GetTimestamp

`func (o *ReportField) GetTimestamp() int32`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *ReportField) GetTimestampOk() (*int32, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *ReportField) SetTimestamp(v int32)`

SetTimestamp sets Timestamp field to given value.


### GetHash

`func (o *ReportField) GetHash() string`

GetHash returns the Hash field if non-nil, zero value otherwise.

### GetHashOk

`func (o *ReportField) GetHashOk() (*string, bool)`

GetHashOk returns a tuple with the Hash field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHash

`func (o *ReportField) SetHash(v string)`

SetHash sets Hash field to given value.


### GetAction

`func (o *ReportField) GetAction() string`

GetAction returns the Action field if non-nil, zero value otherwise.

### GetActionOk

`func (o *ReportField) GetActionOk() (*string, bool)`

GetActionOk returns a tuple with the Action field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAction

`func (o *ReportField) SetAction(v string)`

SetAction sets Action field to given value.


### GetValue

`func (o *ReportField) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *ReportField) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *ReportField) SetValue(v string)`

SetValue sets Value field to given value.


### GetSenderAddress

`func (o *ReportField) GetSenderAddress() string`

GetSenderAddress returns the SenderAddress field if non-nil, zero value otherwise.

### GetSenderAddressOk

`func (o *ReportField) GetSenderAddressOk() (*string, bool)`

GetSenderAddressOk returns a tuple with the SenderAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSenderAddress

`func (o *ReportField) SetSenderAddress(v string)`

SetSenderAddress sets SenderAddress field to given value.


### GetFee

`func (o *ReportField) GetFee() string`

GetFee returns the Fee field if non-nil, zero value otherwise.

### GetFeeOk

`func (o *ReportField) GetFeeOk() (*string, bool)`

GetFeeOk returns a tuple with the Fee field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFee

`func (o *ReportField) SetFee(v string)`

SetFee sets Fee field to given value.


### GetDecimals

`func (o *ReportField) GetDecimals() int32`

GetDecimals returns the Decimals field if non-nil, zero value otherwise.

### GetDecimalsOk

`func (o *ReportField) GetDecimalsOk() (*int32, bool)`

GetDecimalsOk returns a tuple with the Decimals field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDecimals

`func (o *ReportField) SetDecimals(v int32)`

SetDecimals sets Decimals field to given value.


### GetMeta

`func (o *ReportField) GetMeta() ReportFieldMeta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *ReportField) GetMetaOk() (*ReportFieldMeta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *ReportField) SetMeta(v ReportFieldMeta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *ReportField) HasMeta() bool`

HasMeta returns a boolean if a field has been set.

### SetMetaNil

`func (o *ReportField) SetMetaNil(b bool)`

 SetMetaNil sets the value for Meta to be an explicit nil

### UnsetMeta
`func (o *ReportField) UnsetMeta()`

UnsetMeta ensures that no value is present for Meta, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


