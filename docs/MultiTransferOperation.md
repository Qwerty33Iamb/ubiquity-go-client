# MultiTransferOperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | [default to "multi_transfer"]
**Event** | Pointer to **string** |  | [optional] 
**Detail** | [**MultiTransfer**](MultiTransfer.md) |  | 

## Methods

### NewMultiTransferOperation

`func NewMultiTransferOperation(type_ string, detail MultiTransfer, ) *MultiTransferOperation`

NewMultiTransferOperation instantiates a new MultiTransferOperation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewMultiTransferOperationWithDefaults

`func NewMultiTransferOperationWithDefaults() *MultiTransferOperation`

NewMultiTransferOperationWithDefaults instantiates a new MultiTransferOperation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *MultiTransferOperation) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *MultiTransferOperation) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *MultiTransferOperation) SetType(v string)`

SetType sets Type field to given value.


### GetEvent

`func (o *MultiTransferOperation) GetEvent() string`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *MultiTransferOperation) GetEventOk() (*string, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *MultiTransferOperation) SetEvent(v string)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *MultiTransferOperation) HasEvent() bool`

HasEvent returns a boolean if a field has been set.

### GetDetail

`func (o *MultiTransferOperation) GetDetail() MultiTransfer`

GetDetail returns the Detail field if non-nil, zero value otherwise.

### GetDetailOk

`func (o *MultiTransferOperation) GetDetailOk() (*MultiTransfer, bool)`

GetDetailOk returns a tuple with the Detail field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDetail

`func (o *MultiTransferOperation) SetDetail(v MultiTransfer)`

SetDetail sets Detail field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


