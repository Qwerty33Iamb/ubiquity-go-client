# Utxo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssetPath** | Pointer to **string** | Asset path of transferred currency | [optional] 
**Address** | Pointer to **string** |  | [optional] 
**Value** | Pointer to **string** | Integer string in smallest unit (Satoshis) | [optional] 

## Methods

### NewUtxo

`func NewUtxo() *Utxo`

NewUtxo instantiates a new Utxo object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewUtxoWithDefaults

`func NewUtxoWithDefaults() *Utxo`

NewUtxoWithDefaults instantiates a new Utxo object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAssetPath

`func (o *Utxo) GetAssetPath() string`

GetAssetPath returns the AssetPath field if non-nil, zero value otherwise.

### GetAssetPathOk

`func (o *Utxo) GetAssetPathOk() (*string, bool)`

GetAssetPathOk returns a tuple with the AssetPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssetPath

`func (o *Utxo) SetAssetPath(v string)`

SetAssetPath sets AssetPath field to given value.

### HasAssetPath

`func (o *Utxo) HasAssetPath() bool`

HasAssetPath returns a boolean if a field has been set.

### GetAddress

`func (o *Utxo) GetAddress() string`

GetAddress returns the Address field if non-nil, zero value otherwise.

### GetAddressOk

`func (o *Utxo) GetAddressOk() (*string, bool)`

GetAddressOk returns a tuple with the Address field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddress

`func (o *Utxo) SetAddress(v string)`

SetAddress sets Address field to given value.

### HasAddress

`func (o *Utxo) HasAddress() bool`

HasAddress returns a boolean if a field has been set.

### GetValue

`func (o *Utxo) GetValue() string`

GetValue returns the Value field if non-nil, zero value otherwise.

### GetValueOk

`func (o *Utxo) GetValueOk() (*string, bool)`

GetValueOk returns a tuple with the Value field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetValue

`func (o *Utxo) SetValue(v string)`

SetValue sets Value field to given value.

### HasValue

`func (o *Utxo) HasValue() bool`

HasValue returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


