# TokenCurrency

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**AssetPath** | **string** | Asset path of transferred currency | 
**Symbol** | Pointer to **string** | Currency symbol | [optional] 
**Name** | Pointer to **string** | Name of currency | [optional] 
**Decimals** | Pointer to **int32** | Decimal places right to the comma | [optional] 
**Type** | **string** |  | [default to "token"]
**Detail** | Pointer to [**Token**](Token.md) |  | [optional] 

## Methods

### NewTokenCurrency

`func NewTokenCurrency(assetPath string, type_ string, ) *TokenCurrency`

NewTokenCurrency instantiates a new TokenCurrency object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTokenCurrencyWithDefaults

`func NewTokenCurrencyWithDefaults() *TokenCurrency`

NewTokenCurrencyWithDefaults instantiates a new TokenCurrency object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetAssetPath

`func (o *TokenCurrency) GetAssetPath() string`

GetAssetPath returns the AssetPath field if non-nil, zero value otherwise.

### GetAssetPathOk

`func (o *TokenCurrency) GetAssetPathOk() (*string, bool)`

GetAssetPathOk returns a tuple with the AssetPath field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssetPath

`func (o *TokenCurrency) SetAssetPath(v string)`

SetAssetPath sets AssetPath field to given value.


### GetSymbol

`func (o *TokenCurrency) GetSymbol() string`

GetSymbol returns the Symbol field if non-nil, zero value otherwise.

### GetSymbolOk

`func (o *TokenCurrency) GetSymbolOk() (*string, bool)`

GetSymbolOk returns a tuple with the Symbol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSymbol

`func (o *TokenCurrency) SetSymbol(v string)`

SetSymbol sets Symbol field to given value.

### HasSymbol

`func (o *TokenCurrency) HasSymbol() bool`

HasSymbol returns a boolean if a field has been set.

### GetName

`func (o *TokenCurrency) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *TokenCurrency) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *TokenCurrency) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *TokenCurrency) HasName() bool`

HasName returns a boolean if a field has been set.

### GetDecimals

`func (o *TokenCurrency) GetDecimals() int32`

GetDecimals returns the Decimals field if non-nil, zero value otherwise.

### GetDecimalsOk

`func (o *TokenCurrency) GetDecimalsOk() (*int32, bool)`

GetDecimalsOk returns a tuple with the Decimals field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDecimals

`func (o *TokenCurrency) SetDecimals(v int32)`

SetDecimals sets Decimals field to given value.

### HasDecimals

`func (o *TokenCurrency) HasDecimals() bool`

HasDecimals returns a boolean if a field has been set.

### GetType

`func (o *TokenCurrency) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *TokenCurrency) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *TokenCurrency) SetType(v string)`

SetType sets Type field to given value.


### GetDetail

`func (o *TokenCurrency) GetDetail() Token`

GetDetail returns the Detail field if non-nil, zero value otherwise.

### GetDetailOk

`func (o *TokenCurrency) GetDetailOk() (*Token, bool)`

GetDetailOk returns a tuple with the Detail field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDetail

`func (o *TokenCurrency) SetDetail(v Token)`

SetDetail sets Detail field to given value.

### HasDetail

`func (o *TokenCurrency) HasDetail() bool`

HasDetail returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


