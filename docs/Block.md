# Block

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Number** | Pointer to **int64** | Block number | [optional] 
**Id** | Pointer to **string** | Block hash | [optional] 
**ParentId** | Pointer to **string** | Parent block hash | [optional] 
**Date** | Pointer to **int64** | Unix timestamp | [optional] 
**TxIds** | Pointer to **[]string** | Complete list of transaction IDs | [optional] 
**Txs** | Pointer to [**[]Tx**](Tx.md) | Partial list of normalized transactions (not filtered or unknown model) | [optional] 
**Supply** | Pointer to [**map[string]Supply**](Supply.md) | Coin supplies with asset paths as keys | [optional] 

## Methods

### NewBlock

`func NewBlock() *Block`

NewBlock instantiates a new Block object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBlockWithDefaults

`func NewBlockWithDefaults() *Block`

NewBlockWithDefaults instantiates a new Block object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNumber

`func (o *Block) GetNumber() int64`

GetNumber returns the Number field if non-nil, zero value otherwise.

### GetNumberOk

`func (o *Block) GetNumberOk() (*int64, bool)`

GetNumberOk returns a tuple with the Number field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumber

`func (o *Block) SetNumber(v int64)`

SetNumber sets Number field to given value.

### HasNumber

`func (o *Block) HasNumber() bool`

HasNumber returns a boolean if a field has been set.

### GetId

`func (o *Block) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Block) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Block) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Block) HasId() bool`

HasId returns a boolean if a field has been set.

### GetParentId

`func (o *Block) GetParentId() string`

GetParentId returns the ParentId field if non-nil, zero value otherwise.

### GetParentIdOk

`func (o *Block) GetParentIdOk() (*string, bool)`

GetParentIdOk returns a tuple with the ParentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentId

`func (o *Block) SetParentId(v string)`

SetParentId sets ParentId field to given value.

### HasParentId

`func (o *Block) HasParentId() bool`

HasParentId returns a boolean if a field has been set.

### GetDate

`func (o *Block) GetDate() int64`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *Block) GetDateOk() (*int64, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *Block) SetDate(v int64)`

SetDate sets Date field to given value.

### HasDate

`func (o *Block) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetTxIds

`func (o *Block) GetTxIds() []string`

GetTxIds returns the TxIds field if non-nil, zero value otherwise.

### GetTxIdsOk

`func (o *Block) GetTxIdsOk() (*[]string, bool)`

GetTxIdsOk returns a tuple with the TxIds field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTxIds

`func (o *Block) SetTxIds(v []string)`

SetTxIds sets TxIds field to given value.

### HasTxIds

`func (o *Block) HasTxIds() bool`

HasTxIds returns a boolean if a field has been set.

### GetTxs

`func (o *Block) GetTxs() []Tx`

GetTxs returns the Txs field if non-nil, zero value otherwise.

### GetTxsOk

`func (o *Block) GetTxsOk() (*[]Tx, bool)`

GetTxsOk returns a tuple with the Txs field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTxs

`func (o *Block) SetTxs(v []Tx)`

SetTxs sets Txs field to given value.

### HasTxs

`func (o *Block) HasTxs() bool`

HasTxs returns a boolean if a field has been set.

### GetSupply

`func (o *Block) GetSupply() map[string]Supply`

GetSupply returns the Supply field if non-nil, zero value otherwise.

### GetSupplyOk

`func (o *Block) GetSupplyOk() (*map[string]Supply, bool)`

GetSupplyOk returns a tuple with the Supply field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSupply

`func (o *Block) SetSupply(v map[string]Supply)`

SetSupply sets Supply field to given value.

### HasSupply

`func (o *Block) HasSupply() bool`

HasSupply returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


