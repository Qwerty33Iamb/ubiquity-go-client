# BalanceChange

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Old** | Pointer to **string** | Balance before transaction | [optional] 
**Delta** | Pointer to **string** | Balance difference | [optional] 
**New** | Pointer to **string** | Balance after transaction | [optional] 
**Currency** | Pointer to [**Currency**](Currency.md) |  | [optional] 

## Methods

### NewBalanceChange

`func NewBalanceChange() *BalanceChange`

NewBalanceChange instantiates a new BalanceChange object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBalanceChangeWithDefaults

`func NewBalanceChangeWithDefaults() *BalanceChange`

NewBalanceChangeWithDefaults instantiates a new BalanceChange object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetOld

`func (o *BalanceChange) GetOld() string`

GetOld returns the Old field if non-nil, zero value otherwise.

### GetOldOk

`func (o *BalanceChange) GetOldOk() (*string, bool)`

GetOldOk returns a tuple with the Old field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOld

`func (o *BalanceChange) SetOld(v string)`

SetOld sets Old field to given value.

### HasOld

`func (o *BalanceChange) HasOld() bool`

HasOld returns a boolean if a field has been set.

### GetDelta

`func (o *BalanceChange) GetDelta() string`

GetDelta returns the Delta field if non-nil, zero value otherwise.

### GetDeltaOk

`func (o *BalanceChange) GetDeltaOk() (*string, bool)`

GetDeltaOk returns a tuple with the Delta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDelta

`func (o *BalanceChange) SetDelta(v string)`

SetDelta sets Delta field to given value.

### HasDelta

`func (o *BalanceChange) HasDelta() bool`

HasDelta returns a boolean if a field has been set.

### GetNew

`func (o *BalanceChange) GetNew() string`

GetNew returns the New field if non-nil, zero value otherwise.

### GetNewOk

`func (o *BalanceChange) GetNewOk() (*string, bool)`

GetNewOk returns a tuple with the New field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNew

`func (o *BalanceChange) SetNew(v string)`

SetNew sets New field to given value.

### HasNew

`func (o *BalanceChange) HasNew() bool`

HasNew returns a boolean if a field has been set.

### GetCurrency

`func (o *BalanceChange) GetCurrency() Currency`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *BalanceChange) GetCurrencyOk() (*Currency, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *BalanceChange) SetCurrency(v Currency)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *BalanceChange) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


