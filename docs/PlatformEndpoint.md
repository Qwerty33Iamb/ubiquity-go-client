# PlatformEndpoint

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Path** | Pointer to **string** |  | [optional] 
**Example** | Pointer to **string** |  | [optional] 

## Methods

### NewPlatformEndpoint

`func NewPlatformEndpoint() *PlatformEndpoint`

NewPlatformEndpoint instantiates a new PlatformEndpoint object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPlatformEndpointWithDefaults

`func NewPlatformEndpointWithDefaults() *PlatformEndpoint`

NewPlatformEndpointWithDefaults instantiates a new PlatformEndpoint object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetPath

`func (o *PlatformEndpoint) GetPath() string`

GetPath returns the Path field if non-nil, zero value otherwise.

### GetPathOk

`func (o *PlatformEndpoint) GetPathOk() (*string, bool)`

GetPathOk returns a tuple with the Path field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPath

`func (o *PlatformEndpoint) SetPath(v string)`

SetPath sets Path field to given value.

### HasPath

`func (o *PlatformEndpoint) HasPath() bool`

HasPath returns a boolean if a field has been set.

### GetExample

`func (o *PlatformEndpoint) GetExample() string`

GetExample returns the Example field if non-nil, zero value otherwise.

### GetExampleOk

`func (o *PlatformEndpoint) GetExampleOk() (*string, bool)`

GetExampleOk returns a tuple with the Example field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetExample

`func (o *PlatformEndpoint) SetExample(v string)`

SetExample sets Example field to given value.

### HasExample

`func (o *PlatformEndpoint) HasExample() bool`

HasExample returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


