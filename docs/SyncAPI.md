# \SyncAPI

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CurrentBlockID**](SyncAPI.md#CurrentBlockID) | **Get** /{platform}/{network}/sync/block_id | Get current block ID
[**CurrentBlockNumber**](SyncAPI.md#CurrentBlockNumber) | **Get** /{platform}/{network}/sync/block_number | Get current block number



## CurrentBlockID

> string CurrentBlockID(ctx, platform, network).Execute()

Get current block ID

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.SyncAPI.CurrentBlockID(context.Background(), platform, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SyncAPI.CurrentBlockID``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CurrentBlockID`: string
    fmt.Fprintf(os.Stdout, "Response from `SyncAPI.CurrentBlockID`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiCurrentBlockIDRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**string**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## CurrentBlockNumber

> int64 CurrentBlockNumber(ctx, platform, network).Execute()

Get current block number

### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.SyncAPI.CurrentBlockNumber(context.Background(), platform, network).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `SyncAPI.CurrentBlockNumber``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `CurrentBlockNumber`: int64
    fmt.Fprintf(os.Stdout, "Response from `SyncAPI.CurrentBlockNumber`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 

### Other Parameters

Other parameters are passed through a pointer to a apiCurrentBlockNumberRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------



### Return type

**int64**

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

