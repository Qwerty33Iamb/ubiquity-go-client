# Tx

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **string** | Unique transaction identifier | [optional] 
**Slip44** | Pointer to **int32** | SLIP-44 coin ID | [optional] 
**Addresses** | Pointer to **[]string** | List of involved addresses (excluding contracts) | [optional] 
**Assets** | Pointer to **[]string** | List of moved assets by asset path | [optional] 
**Date** | Pointer to **int64** | Unix timestamp | [optional] 
**Height** | Pointer to **NullableInt64** | Number of block if mined, otherwise omitted. | [optional] 
**BlockId** | Pointer to **NullableString** | ID of block if mined, otherwise omitted. | [optional] 
**Status** | Pointer to **string** | Result status of the transaction. | [optional] 
**Tags** | Pointer to **[]string** | List of tags for this transaction | [optional] 
**Operations** | Pointer to [**map[string]Operation**](Operation.md) | Operations in this transaction (opaque keys). | [optional] 
**Effects** | Pointer to [**map[string]Effect**](Effect.md) | Effects by address, if supported | [optional] 

## Methods

### NewTx

`func NewTx() *Tx`

NewTx instantiates a new Tx object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxWithDefaults

`func NewTxWithDefaults() *Tx`

NewTxWithDefaults instantiates a new Tx object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Tx) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Tx) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Tx) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *Tx) HasId() bool`

HasId returns a boolean if a field has been set.

### GetSlip44

`func (o *Tx) GetSlip44() int32`

GetSlip44 returns the Slip44 field if non-nil, zero value otherwise.

### GetSlip44Ok

`func (o *Tx) GetSlip44Ok() (*int32, bool)`

GetSlip44Ok returns a tuple with the Slip44 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSlip44

`func (o *Tx) SetSlip44(v int32)`

SetSlip44 sets Slip44 field to given value.

### HasSlip44

`func (o *Tx) HasSlip44() bool`

HasSlip44 returns a boolean if a field has been set.

### GetAddresses

`func (o *Tx) GetAddresses() []string`

GetAddresses returns the Addresses field if non-nil, zero value otherwise.

### GetAddressesOk

`func (o *Tx) GetAddressesOk() (*[]string, bool)`

GetAddressesOk returns a tuple with the Addresses field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAddresses

`func (o *Tx) SetAddresses(v []string)`

SetAddresses sets Addresses field to given value.

### HasAddresses

`func (o *Tx) HasAddresses() bool`

HasAddresses returns a boolean if a field has been set.

### SetAddressesNil

`func (o *Tx) SetAddressesNil(b bool)`

 SetAddressesNil sets the value for Addresses to be an explicit nil

### UnsetAddresses
`func (o *Tx) UnsetAddresses()`

UnsetAddresses ensures that no value is present for Addresses, not even an explicit nil
### GetAssets

`func (o *Tx) GetAssets() []string`

GetAssets returns the Assets field if non-nil, zero value otherwise.

### GetAssetsOk

`func (o *Tx) GetAssetsOk() (*[]string, bool)`

GetAssetsOk returns a tuple with the Assets field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAssets

`func (o *Tx) SetAssets(v []string)`

SetAssets sets Assets field to given value.

### HasAssets

`func (o *Tx) HasAssets() bool`

HasAssets returns a boolean if a field has been set.

### SetAssetsNil

`func (o *Tx) SetAssetsNil(b bool)`

 SetAssetsNil sets the value for Assets to be an explicit nil

### UnsetAssets
`func (o *Tx) UnsetAssets()`

UnsetAssets ensures that no value is present for Assets, not even an explicit nil
### GetDate

`func (o *Tx) GetDate() int64`

GetDate returns the Date field if non-nil, zero value otherwise.

### GetDateOk

`func (o *Tx) GetDateOk() (*int64, bool)`

GetDateOk returns a tuple with the Date field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDate

`func (o *Tx) SetDate(v int64)`

SetDate sets Date field to given value.

### HasDate

`func (o *Tx) HasDate() bool`

HasDate returns a boolean if a field has been set.

### GetHeight

`func (o *Tx) GetHeight() int64`

GetHeight returns the Height field if non-nil, zero value otherwise.

### GetHeightOk

`func (o *Tx) GetHeightOk() (*int64, bool)`

GetHeightOk returns a tuple with the Height field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHeight

`func (o *Tx) SetHeight(v int64)`

SetHeight sets Height field to given value.

### HasHeight

`func (o *Tx) HasHeight() bool`

HasHeight returns a boolean if a field has been set.

### SetHeightNil

`func (o *Tx) SetHeightNil(b bool)`

 SetHeightNil sets the value for Height to be an explicit nil

### UnsetHeight
`func (o *Tx) UnsetHeight()`

UnsetHeight ensures that no value is present for Height, not even an explicit nil
### GetBlockId

`func (o *Tx) GetBlockId() string`

GetBlockId returns the BlockId field if non-nil, zero value otherwise.

### GetBlockIdOk

`func (o *Tx) GetBlockIdOk() (*string, bool)`

GetBlockIdOk returns a tuple with the BlockId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockId

`func (o *Tx) SetBlockId(v string)`

SetBlockId sets BlockId field to given value.

### HasBlockId

`func (o *Tx) HasBlockId() bool`

HasBlockId returns a boolean if a field has been set.

### SetBlockIdNil

`func (o *Tx) SetBlockIdNil(b bool)`

 SetBlockIdNil sets the value for BlockId to be an explicit nil

### UnsetBlockId
`func (o *Tx) UnsetBlockId()`

UnsetBlockId ensures that no value is present for BlockId, not even an explicit nil
### GetStatus

`func (o *Tx) GetStatus() string`

GetStatus returns the Status field if non-nil, zero value otherwise.

### GetStatusOk

`func (o *Tx) GetStatusOk() (*string, bool)`

GetStatusOk returns a tuple with the Status field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStatus

`func (o *Tx) SetStatus(v string)`

SetStatus sets Status field to given value.

### HasStatus

`func (o *Tx) HasStatus() bool`

HasStatus returns a boolean if a field has been set.

### GetTags

`func (o *Tx) GetTags() []string`

GetTags returns the Tags field if non-nil, zero value otherwise.

### GetTagsOk

`func (o *Tx) GetTagsOk() (*[]string, bool)`

GetTagsOk returns a tuple with the Tags field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTags

`func (o *Tx) SetTags(v []string)`

SetTags sets Tags field to given value.

### HasTags

`func (o *Tx) HasTags() bool`

HasTags returns a boolean if a field has been set.

### SetTagsNil

`func (o *Tx) SetTagsNil(b bool)`

 SetTagsNil sets the value for Tags to be an explicit nil

### UnsetTags
`func (o *Tx) UnsetTags()`

UnsetTags ensures that no value is present for Tags, not even an explicit nil
### GetOperations

`func (o *Tx) GetOperations() map[string]Operation`

GetOperations returns the Operations field if non-nil, zero value otherwise.

### GetOperationsOk

`func (o *Tx) GetOperationsOk() (*map[string]Operation, bool)`

GetOperationsOk returns a tuple with the Operations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOperations

`func (o *Tx) SetOperations(v map[string]Operation)`

SetOperations sets Operations field to given value.

### HasOperations

`func (o *Tx) HasOperations() bool`

HasOperations returns a boolean if a field has been set.

### GetEffects

`func (o *Tx) GetEffects() map[string]Effect`

GetEffects returns the Effects field if non-nil, zero value otherwise.

### GetEffectsOk

`func (o *Tx) GetEffectsOk() (*map[string]Effect, bool)`

GetEffectsOk returns a tuple with the Effects field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEffects

`func (o *Tx) SetEffects(v map[string]Effect)`

SetEffects sets Effects field to given value.

### HasEffects

`func (o *Tx) HasEffects() bool`

HasEffects returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


