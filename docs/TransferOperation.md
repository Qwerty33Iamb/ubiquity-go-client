# TransferOperation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** |  | [default to "transfer"]
**Event** | Pointer to **string** |  | [optional] 
**Detail** | [**Transfer**](Transfer.md) |  | 

## Methods

### NewTransferOperation

`func NewTransferOperation(type_ string, detail Transfer, ) *TransferOperation`

NewTransferOperation instantiates a new TransferOperation object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTransferOperationWithDefaults

`func NewTransferOperationWithDefaults() *TransferOperation`

NewTransferOperationWithDefaults instantiates a new TransferOperation object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *TransferOperation) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *TransferOperation) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *TransferOperation) SetType(v string)`

SetType sets Type field to given value.


### GetEvent

`func (o *TransferOperation) GetEvent() string`

GetEvent returns the Event field if non-nil, zero value otherwise.

### GetEventOk

`func (o *TransferOperation) GetEventOk() (*string, bool)`

GetEventOk returns a tuple with the Event field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEvent

`func (o *TransferOperation) SetEvent(v string)`

SetEvent sets Event field to given value.

### HasEvent

`func (o *TransferOperation) HasEvent() bool`

HasEvent returns a boolean if a field has been set.

### GetDetail

`func (o *TransferOperation) GetDetail() Transfer`

GetDetail returns the Detail field if non-nil, zero value otherwise.

### GetDetailOk

`func (o *TransferOperation) GetDetailOk() (*Transfer, bool)`

GetDetailOk returns a tuple with the Detail field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetDetail

`func (o *TransferOperation) SetDetail(v Transfer)`

SetDetail sets Detail field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


