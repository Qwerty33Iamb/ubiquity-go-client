# BlockIdentifier

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Number** | Pointer to **int64** | Block number | [optional] 
**Id** | Pointer to **string** | Block hash | [optional] 
**ParentId** | Pointer to **string** | Parent block hash | [optional] 

## Methods

### NewBlockIdentifier

`func NewBlockIdentifier() *BlockIdentifier`

NewBlockIdentifier instantiates a new BlockIdentifier object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBlockIdentifierWithDefaults

`func NewBlockIdentifierWithDefaults() *BlockIdentifier`

NewBlockIdentifierWithDefaults instantiates a new BlockIdentifier object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetNumber

`func (o *BlockIdentifier) GetNumber() int64`

GetNumber returns the Number field if non-nil, zero value otherwise.

### GetNumberOk

`func (o *BlockIdentifier) GetNumberOk() (*int64, bool)`

GetNumberOk returns a tuple with the Number field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetNumber

`func (o *BlockIdentifier) SetNumber(v int64)`

SetNumber sets Number field to given value.

### HasNumber

`func (o *BlockIdentifier) HasNumber() bool`

HasNumber returns a boolean if a field has been set.

### GetId

`func (o *BlockIdentifier) GetId() string`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *BlockIdentifier) GetIdOk() (*string, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *BlockIdentifier) SetId(v string)`

SetId sets Id field to given value.

### HasId

`func (o *BlockIdentifier) HasId() bool`

HasId returns a boolean if a field has been set.

### GetParentId

`func (o *BlockIdentifier) GetParentId() string`

GetParentId returns the ParentId field if non-nil, zero value otherwise.

### GetParentIdOk

`func (o *BlockIdentifier) GetParentIdOk() (*string, bool)`

GetParentIdOk returns a tuple with the ParentId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetParentId

`func (o *BlockIdentifier) SetParentId(v string)`

SetParentId sets ParentId field to given value.

### HasParentId

`func (o *BlockIdentifier) HasParentId() bool`

HasParentId returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


