# \BlocksAPI

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetBlock**](BlocksAPI.md#GetBlock) | **Get** /{platform}/{network}/block/{key} | Block By Number/Hash
[**GetBlockIdentifier**](BlocksAPI.md#GetBlockIdentifier) | **Get** /{platform}/{network}/block_identifier/{key} | Block Identifier By Number/Hash



## GetBlock

> Block GetBlock(ctx, platform, network, key).Execute()

Block By Number/Hash



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    key := "8000000" // string | Block number or block hash/ID or Special identifier

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetBlock(context.Background(), platform, network, key).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetBlock``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBlock`: Block
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetBlock`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**key** | **string** | Block number or block hash/ID or Special identifier | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBlockRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**Block**](Block.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetBlockIdentifier

> BlockIdentifier GetBlockIdentifier(ctx, platform, network, key).Execute()

Block Identifier By Number/Hash



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    platform := "bitcoin" // string | Coin platform handle
    network := "mainnet" // string | Which network to target. Available networks can be found with /{platform}
    key := "8000000" // string | Block number or block hash/ID or Special identifier

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.BlocksAPI.GetBlockIdentifier(context.Background(), platform, network, key).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `BlocksAPI.GetBlockIdentifier``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `GetBlockIdentifier`: BlockIdentifier
    fmt.Fprintf(os.Stdout, "Response from `BlocksAPI.GetBlockIdentifier`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**platform** | **string** | Coin platform handle | 
**network** | **string** | Which network to target. Available networks can be found with /{platform} | 
**key** | **string** | Block number or block hash/ID or Special identifier | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetBlockIdentifierRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**BlockIdentifier**](BlockIdentifier.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

