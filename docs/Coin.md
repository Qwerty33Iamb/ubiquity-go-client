# Coin

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Handle** | Pointer to **string** | Platform handle (platform var in path) | [optional] 
**Slip44** | Pointer to **int32** | SatoshiLabs 0044, registered coin types: https://github.com/satoshilabs/slips/blob/master/slip-0044.md | [optional] 
**Symbol** | Pointer to **string** | Symbol of native currency | [optional] 
**Name** | Pointer to **string** | Name of platform | [optional] 
**BlockTime** | Pointer to **int32** | Average time between blocks (milliseconds) | [optional] 
**SampleAddress** | Pointer to **string** | Random address seen on chain (optional) | [optional] 

## Methods

### NewCoin

`func NewCoin() *Coin`

NewCoin instantiates a new Coin object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewCoinWithDefaults

`func NewCoinWithDefaults() *Coin`

NewCoinWithDefaults instantiates a new Coin object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetHandle

`func (o *Coin) GetHandle() string`

GetHandle returns the Handle field if non-nil, zero value otherwise.

### GetHandleOk

`func (o *Coin) GetHandleOk() (*string, bool)`

GetHandleOk returns a tuple with the Handle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHandle

`func (o *Coin) SetHandle(v string)`

SetHandle sets Handle field to given value.

### HasHandle

`func (o *Coin) HasHandle() bool`

HasHandle returns a boolean if a field has been set.

### GetSlip44

`func (o *Coin) GetSlip44() int32`

GetSlip44 returns the Slip44 field if non-nil, zero value otherwise.

### GetSlip44Ok

`func (o *Coin) GetSlip44Ok() (*int32, bool)`

GetSlip44Ok returns a tuple with the Slip44 field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSlip44

`func (o *Coin) SetSlip44(v int32)`

SetSlip44 sets Slip44 field to given value.

### HasSlip44

`func (o *Coin) HasSlip44() bool`

HasSlip44 returns a boolean if a field has been set.

### GetSymbol

`func (o *Coin) GetSymbol() string`

GetSymbol returns the Symbol field if non-nil, zero value otherwise.

### GetSymbolOk

`func (o *Coin) GetSymbolOk() (*string, bool)`

GetSymbolOk returns a tuple with the Symbol field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSymbol

`func (o *Coin) SetSymbol(v string)`

SetSymbol sets Symbol field to given value.

### HasSymbol

`func (o *Coin) HasSymbol() bool`

HasSymbol returns a boolean if a field has been set.

### GetName

`func (o *Coin) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Coin) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Coin) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *Coin) HasName() bool`

HasName returns a boolean if a field has been set.

### GetBlockTime

`func (o *Coin) GetBlockTime() int32`

GetBlockTime returns the BlockTime field if non-nil, zero value otherwise.

### GetBlockTimeOk

`func (o *Coin) GetBlockTimeOk() (*int32, bool)`

GetBlockTimeOk returns a tuple with the BlockTime field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBlockTime

`func (o *Coin) SetBlockTime(v int32)`

SetBlockTime sets BlockTime field to given value.

### HasBlockTime

`func (o *Coin) HasBlockTime() bool`

HasBlockTime returns a boolean if a field has been set.

### GetSampleAddress

`func (o *Coin) GetSampleAddress() string`

GetSampleAddress returns the SampleAddress field if non-nil, zero value otherwise.

### GetSampleAddressOk

`func (o *Coin) GetSampleAddressOk() (*string, bool)`

GetSampleAddressOk returns a tuple with the SampleAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSampleAddress

`func (o *Coin) SetSampleAddress(v string)`

SetSampleAddress sets SampleAddress field to given value.

### HasSampleAddress

`func (o *Coin) HasSampleAddress() bool`

HasSampleAddress returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


