# Effect

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**BalanceChanges** | Pointer to [**map[string]BalanceChange**](BalanceChange.md) | Balance changes by asset | [optional] 

## Methods

### NewEffect

`func NewEffect() *Effect`

NewEffect instantiates a new Effect object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewEffectWithDefaults

`func NewEffectWithDefaults() *Effect`

NewEffectWithDefaults instantiates a new Effect object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetBalanceChanges

`func (o *Effect) GetBalanceChanges() map[string]BalanceChange`

GetBalanceChanges returns the BalanceChanges field if non-nil, zero value otherwise.

### GetBalanceChangesOk

`func (o *Effect) GetBalanceChangesOk() (*map[string]BalanceChange, bool)`

GetBalanceChangesOk returns a tuple with the BalanceChanges field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetBalanceChanges

`func (o *Effect) SetBalanceChanges(v map[string]BalanceChange)`

SetBalanceChanges sets BalanceChanges field to given value.

### HasBalanceChanges

`func (o *Effect) HasBalanceChanges() bool`

HasBalanceChanges returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


