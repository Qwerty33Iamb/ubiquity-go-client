.PHONY: all generate clean build test

default_openapi_jar_path = openapi-generator-cli.jar
ifeq "$(OPENAPI_GENERATOR_JAR_PATH)" ""
	openapi_jar_path := $(default_openapi_jar_path)
else
	openapi_jar_path := $(OPENAPI_GENERATOR_JAR_PATH)
endif

default_client_gen_path = pkg/client
ifeq "$(CLIENT_GEN_PATH)" ""
	client_gen_path := $(default_client_gen_path)
else
	client_gen_path := $(CLIENT_GEN_PATH)
endif

all: clean generate build test
generate:
	@echo "Generating code..."
	java -jar $(openapi_jar_path) generate -i spec/openapi.yaml -c open-api-conf.yaml -g go -o $(client_gen_path)
	rm $(openapi_jar_path)
	rm -rf $(client_gen_path)/.openapi-generator
	mv $(client_gen_path)/docs .
	mv $(client_gen_path)/go.mod .
	mv $(client_gen_path)/go.sum .
	go mod tidy -v # This is needed to fix go.sum
	go fmt ./...

clean:
	@echo "Cleaning up..."
	rm -rf docs
	rm $(client_gen_path)/*.go go.mod go.sum

build:
	go build ./...

build-examples:
	cd examples/current-block && go mod tidy -v && go build && go clean && cd ../..
	cd examples/tx-pagination && go mod tidy -v && go build && go clean && cd ../..
	cd examples/tx-querying && go mod tidy -v && go build && go clean && cd ../..
	cd examples/tx-send && go mod tidy -v && go build && go clean && cd ../..
	cd examples/ws-blocks && go mod tidy -v && go build && go clean && cd ../..

test:
	go test ./...