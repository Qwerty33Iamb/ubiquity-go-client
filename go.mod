module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client

go 1.13

require (
	github.com/btcsuite/btcd v0.22.0-beta
	github.com/btcsuite/btcutil v1.0.3-0.20201208143702-a53e38424cce
	github.com/ethereum/go-ethereum v1.10.11
	github.com/gorilla/websocket v1.4.2
	github.com/stretchr/testify v1.7.0
	golang.org/x/oauth2 v0.0.0-20210218202405-ba52d332ba99
)
