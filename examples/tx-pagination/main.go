package main

import (
	"context"
	"fmt"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
	ubiquitypl "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/platform"
	"os"
	"strings"
	"time"
)

/**
Paginating over transactions.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
	3) UBI_PLATFORM - optional, platform e.g. ethereum (bitcoin by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and platform
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)
	var pl string
	if pl = os.Getenv("UBI_PLATFORM"); pl == "" {
		pl = ubiquitypl.Bitcoin
	}

	// Paginating 10 times over transaction pages with limit 10
	var limit int32 = 10
	order := "desc"
	var continuation *string

	fmt.Println("Paginating 10 times over transaction pages with limit 10:")
	fmt.Println()

	for i := 0; i < 10; i++ {
		req := apiClient.TransactionsAPI.GetTxs(ctx, pl, "mainnet").Limit(limit).Order(order)
		if continuation != nil {
			req.Continuation(*continuation)
		}

		page, resp, err := req.Execute()
		if err != nil {
			panic(fmt.Errorf("failed to paginate %s transactions: got status '%s' and error '%v'",
				strings.Title(pl), resp.Status, err))
		}
		fmt.Printf("Got %s transactions page with size %d:\n", strings.Title(pl), len(page.GetItems()))
		printTxsPage(page)

		if continuation = page.Continuation.Get(); continuation == nil {
			break // If we hit end of the list sooner than the loop ends
		}

		fmt.Println()
	}
}

func printTxsPage(page ubiquity.TxPage) {
	for _, tx := range page.GetItems() {
		fmt.Printf("Transaction data: ID = '%s', addresses = %v, assets = %v, date = %v, height = %d, "+
			"status = %s, tags = %v, operations count = %d\n",
			tx.GetId(), tx.GetAddresses(), tx.GetAssets(), time.Unix(tx.GetDate(), 0), tx.GetHeight(),
			tx.GetStatus(), tx.GetTags(), len(tx.GetOperations()))
	}
}
