package main

import (
	"context"
	"flag"
	"fmt"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
	ubiquitypl "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/platform"
	"os"
	"strings"
	"time"
)

/**
Querying transaction by ID.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
	3) UBI_PLATFORM - optional, platform e.g. ethereum (bitcoin by default)

Flags:
  -txID string
        Transaction ID
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and platform
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)
	var pl string
	if pl = os.Getenv("UBI_PLATFORM"); pl == "" {
		pl = ubiquitypl.Bitcoin
	}

	txID := getTxID()
	tx, resp, err := apiClient.TransactionsAPI.GetTx(ctx, pl, "mainnet", txID).Execute()
	if err != nil {
		panic(fmt.Errorf("failed to get %s transaction by ID '%s': got status '%s' and error '%v'",
			strings.Title(pl), txID, resp.Status, err))
	}
	fmt.Printf("Transaction data: ID = '%s', addresses = %v, assets = %v, date = %v, height = %d, "+
		"status = %s, tags = %v, operations = [",
		tx.GetId(), tx.GetAddresses(), tx.GetAssets(), time.Unix(tx.GetDate(), 0), tx.GetHeight(),
		tx.GetStatus(), tx.GetTags())
	for k, op := range tx.GetOperations() {
		actualOp := op.GetActualInstance()
		var detailStr string
		switch actualOp.(type) {
		case *ubiquity.TransferOperation:
			detailStr = transferToStr(actualOp.(*ubiquity.TransferOperation).GetDetail())
		case *ubiquity.MultiTransferOperation:
			detailStr = multiTransferToStr(actualOp.(*ubiquity.MultiTransferOperation).GetDetail())
		default:
			detailStr = fmt.Sprintf("actual instance = %T", actualOp)
		}
		fmt.Printf("{%s => {%v}}, ", k, detailStr)
	}
	fmt.Println("]")
}

func getTxID() string {
	fs := flag.NewFlagSet("", flag.ContinueOnError)
	txID := fs.String("txID", "", "Transaction ID")
	if err := fs.Parse(os.Args[1:]); err != nil {
		panic(fmt.Errorf("failed to parse transaction querying flags: %v", err))
	}
	fs.VisitAll(func(f *flag.Flag) {
		if f.Value.String() == "" {
			fs.SetOutput(os.Stderr)
			_, _ = fmt.Fprintln(os.Stderr, "All of the flags are required:")
			fs.PrintDefaults()
			os.Exit(1)
		}
	})
	return *txID
}

func transferToStr(transfer ubiquity.Transfer) string {
	return fmt.Sprintf("from = %s, value = %s, to = %s, fee = %s, currency = {%s}", transfer.GetFrom(),
		transfer.GetValue(), transfer.GetTo(), transfer.GetFee(), currencyToStr(transfer.GetCurrency()))
}

func multiTransferToStr(multiTransfer ubiquity.MultiTransfer) string {
	return fmt.Sprintf("inputs = [%s], outputs = [%s], total in = %s, total out = %s, unspent = %s, currency = {%s}",
		utxoSliceToStr(multiTransfer.GetInputs()), utxoSliceToStr(multiTransfer.GetOutputs()),
		multiTransfer.GetTotalIn(), multiTransfer.GetTotalOut(), multiTransfer.GetUnspent(),
		currencyToStr(multiTransfer.GetCurrency()))
}

func utxoSliceToStr(utxos []ubiquity.Utxo) string {
	var utxosStr string
	for _, utxo := range utxos {
		utxosStr += fmt.Sprintf("{address = %s, asset path = %s, value = %s}, ", utxo.GetAddress(),
			utxo.GetAssetPath(), utxo.GetValue())
	}
	return utxosStr
}

func currencyToStr(currency ubiquity.Currency) string {
	actualCurr := currency.GetActualInstance()
	switch actualCurr.(type) {
	case *ubiquity.NativeCurrency:
		native := actualCurr.(*ubiquity.NativeCurrency)
		return fmt.Sprintf("type = %s, name = %s, assert path = %s, symbol = %s, decimals %d",
			native.GetType(), native.GetName(), native.GetAssetPath(), native.GetSymbol(), native.GetDecimals())
	case *ubiquity.TokenCurrency:
		token := actualCurr.(*ubiquity.TokenCurrency)
		return fmt.Sprintf("type = %s, name = %s, assert path = %s, symbol = %s, decimals %d, detail = %v",
			token.GetType(), token.GetName(), token.GetAssetPath(), token.GetSymbol(), token.GetDecimals(),
			token.GetDetail())
	case *ubiquity.SmartTokenCurrency:
		smartToken := actualCurr.(*ubiquity.SmartTokenCurrency)
		return fmt.Sprintf("type = %s, name = %s, assert path = %s, symbol = %s, decimals %d, detail = %v",
			smartToken.GetType(), smartToken.GetName(), smartToken.GetAssetPath(), smartToken.GetSymbol(),
			smartToken.GetDecimals(), smartToken.GetDetail())
	default:
		return fmt.Sprintf("actual instance = %T", actualCurr)
	}
}
