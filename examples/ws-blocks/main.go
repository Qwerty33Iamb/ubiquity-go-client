package main

import (
	"fmt"
	ubiquitypl "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/platform"
	ubiquityws "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/ws"
	"os"
	"time"
)

/**
Subscribing to Websocket channel 'ubiquity.blocks'. See https://app.blockdaemon.com/docs/ubiquity#ubiquity-web-sockets-api

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_PLATFORM - optional, platform e.g. ethereum (bitcoin by default)
	3) UBI_NETWORK - optional, network (mainnet by default)
*/

const (
	loopDuration = 10 * time.Minute
)

func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// Platform, BTC by default
	var pl string
	if pl = os.Getenv("UBI_PLATFORM"); pl == "" {
		pl = ubiquitypl.Bitcoin
	}

	network := os.Getenv("UBI_NETWORK") // Will use mainnet if not specified

	wsClient, err := ubiquityws.NewClient(&ubiquityws.Config{
		Platform: pl,
		Network:  network,
		APIKey:   accessToken,
	})
	if err != nil {
		panic(fmt.Errorf("failed to create a WS client: %v", err))
	}
	defer wsClient.Close() // This will automatically unsubscribe all subscriptions

	subID, blocks, err := wsClient.SubscribeBlocks()
	if err != nil {
		panic(fmt.Errorf("failed to subscribe to blocks: %v", err))
	}
	// It's a good practice to unsubscribe if you plan to run WS client for a long time (which is not a case here)
	//defer wsClient.UnsubscribeBlockIDs(subID)

	startedAt := time.Now()
	finishAt := startedAt.Add(loopDuration)
	fmt.Printf("Subscribed to %s blocks under subID #%s\n", pl, subID)
	for {
		select {
		case b, ok := <-blocks:
			if !ok {
				// The subscription would close if we close a client or get disconnected
				fmt.Println("The subscription was closed")
				return
			}
			fmt.Printf("Received a block #%s with number %d and transaction count %d\n", b.GetId(), b.GetNumber(),
				len(b.GetTxs()))
		case <-time.After(finishAt.Sub(time.Now())):
			fmt.Println("Ending the subscription loop.")
			return
		}
	}
}
