package ubiquitypl

/**
Supported platforms aka protocols.
*/
const (
	Bitcoin  = "bitcoin"
	Ethereum = "ethereum"
	Nimiq    = "nimiq"
	Stellar  = "stellar"
	XRP      = "xrp"
	Polkadot = "polkadot"
	Celo     = "celo"
	Algorand = "algorand"
)
