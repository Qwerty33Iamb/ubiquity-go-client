package ubiquityws

import (
	"encoding/json"
	"fmt"
	"math"
	"strings"
)

// rawJSONValues is a convenient type to work with a partially unmarshalled JSON.
type rawJSONValues map[string]json.RawMessage

func (r rawJSONValues) hasKey(key string) bool {
	return len(r[key]) > 0
}

func (r rawJSONValues) getBool(key string) (bool, error) {
	var res bool
	if err := json.Unmarshal(r[key], &res); err != nil {
		return false, fmt.Errorf("failed to unmarshal field '%s': %v", key, err)
	}
	return res, nil
}

func (r rawJSONValues) getString(key string) (string, error) {
	var res string
	if err := json.Unmarshal(r[key], &res); err != nil {
		return "", fmt.Errorf("failed to unmarshal field '%s': %v", key, err)
	}
	return res, nil
}

func (r rawJSONValues) getInt64(key string) (int64, error) {
	var res int64
	if err := json.Unmarshal(r[key], &res); err != nil {
		return 0, fmt.Errorf("failed to unmarshal field '%s': %v", key, err)
	}
	return res, nil
}

func (r rawJSONValues) getForRef(key string, ref interface{}) error {
	if err := json.Unmarshal(r[key], ref); err != nil {
		return fmt.Errorf("failed to unmarshal field '%s': %v", key, err)
	}
	return nil
}

func (r rawJSONValues) String() string {
	var sb strings.Builder
	sb.WriteString("rawJSONValues {")
	for key, val := range r {
		sb.WriteString(fmt.Sprintf("'%s' -> '", key))
		maxTo := math.Min(float64(32), float64(len(val)))
		sb.Write(val[:int(maxTo)])
		if len(val) > int(maxTo) {
			sb.WriteString("...")
		}
		sb.WriteString("', ")
	}
	sb.WriteString("}")
	return sb.String()
}
