package ubiquityws

import (
	"errors"
	"fmt"
	"github.com/gorilla/websocket"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/pkg/client"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	wsURLPattern = "wss://ubiquity.api.blockdaemon.com/v2/%s/%s/websocket"

	blockIDChannel = "ubiquity.block_identifiers"
	blockChannel   = "ubiquity.blocks"
	txChannel      = "ubiquity.txs"

	subscriptionMethod = "ubiquity.subscription"

	defaultPublishTimeoutSec  = 60
	defaultResponseTimeoutSec = 60
)

// Client implements Ubiquity Websocket API - https://app.blockdaemon.com/docs/ubiquity#ubiquity-web-sockets-api
type Client struct {
	logger             *wsLogger
	wsConn             *websocket.Conn
	reqIDCounter       int64
	rMtx               sync.Mutex // Mutex for WS requests, only one write allowed at a time
	pendingRequests    sync.Map
	publishTimeoutSec  int64
	responseTimeoutSec int64
	sMtx               sync.RWMutex // Mutex to protect the below subscribers
	blockIDSubscribers map[string]chan *ubiquity.BlockIdentifier
	blockSubscribers   map[string]chan *ubiquity.Block
	txSubscribers      map[string]chan *ubiquity.Tx
}

type Config struct {
	// You should specify Platform (one of ubiquitypl.*) or WebsocketURL along with APIKey
	Platform           string
	Network            string // mainnet by default
	WebsocketURL       string // You can use one of the constants ubiquityws.Endpoint*
	APIKey             string
	PublishTimeoutSec  int64
	ResponseTimeoutSec int64
	ShowDebug          bool
}

func NewClient(conf *Config) (*Client, error) {
	var wsURL string
	network := "mainnet"
	if conf.Network != "" {
		network = conf.Network
	}

	if pl := conf.Platform; pl != "" {
		wsURL = fmt.Sprintf(wsURLPattern, pl, network)
	} else if conf.WebsocketURL != "" {
		wsURL = conf.WebsocketURL
	} else {
		return nil, errors.New("either Platform or WebsocketURL must be provided")
	}

	if conf.APIKey == "" {
		return nil, errors.New("API key must be provided")
	}
	authHeader := http.Header{"Authorization": []string{"Bearer " + conf.APIKey}}

	conn, resp, err := websocket.DefaultDialer.Dial(wsURL, authHeader)
	if err != nil {
		return nil, fmt.Errorf("failed to connect: err = %v and resp = %v", err, resp)
	}

	publishTimeout := conf.PublishTimeoutSec
	if publishTimeout <= 0 {
		publishTimeout = defaultPublishTimeoutSec
	}
	responseTimeout := conf.ResponseTimeoutSec
	if responseTimeout <= 0 {
		responseTimeout = defaultResponseTimeoutSec
	}

	c := &Client{
		logger: &wsLogger{
			Logger:    log.New(os.Stdout, "ubiquityws.logger ", log.LstdFlags),
			showDebug: conf.ShowDebug,
		},
		wsConn:             conn,
		publishTimeoutSec:  publishTimeout,
		responseTimeoutSec: responseTimeout,
		blockIDSubscribers: make(map[string]chan *ubiquity.BlockIdentifier),
		blockSubscribers:   make(map[string]chan *ubiquity.Block),
		txSubscribers:      make(map[string]chan *ubiquity.Tx),
	}
	go c.runWSMessageProcessing()

	return c, nil
}

func (c *Client) runWSMessageProcessing() {
	for {
		msg := make(rawJSONValues)
		if err := c.wsConn.ReadJSON(&msg); err != nil {
			if strings.Contains(err.Error(), "use of closed network connection") {
				c.logger.info("WS message processing terminated: connection closed") // WS client was closed
			} else {
				c.logger.error("WS message processing terminated due to read error: %v", err)
				go func() {
					c.logger.info("Closing a client...")
					if err := c.Close(); err != nil {
						c.logger.warn("failed to close: %v", err)
					}
				}()
			}
			return
		}

		if err := c.processWSMessage(msg); err != nil {
			c.logger.error("Got error '%v' while processing WS message: %v", err, msg)
		}
	}
}

type aggregatedErr []error

func (err aggregatedErr) Error() string {
	var sb strings.Builder
	sb.WriteString("errors [")
	for _, e := range err {
		_, _ = sb.WriteString(fmt.Sprintf("\"%s\", ", e.Error()))
	}
	sb.WriteString("]")
	return sb.String()
}

func (c *Client) processWSMessage(msg rawJSONValues) error {
	c.logger.debug("Received WS message: %v", msg)
	switch {
	case msg.hasKey("id"):
		resp, err := wsResponseFromRaw(msg)
		if err != nil {
			return err
		}
		reqIDStr := strconv.FormatInt(resp.ID, 10)
		if val, _ := c.pendingRequests.Load(reqIDStr); val != nil {
			respC := val.(chan *wsResponse)
			defer close(respC)
			select {
			case respC <- resp:
				return nil
			case <-time.After(time.Duration(c.responseTimeoutSec) * time.Second):
				return fmt.Errorf("WS response for req. #%s will be dropped: timed out while "+
					"waiting for it to be consumed", reqIDStr)
			}
		} else {
			return fmt.Errorf("unknown Request ID: %s", reqIDStr)
		}
	case msg.hasKey("method"):
		method, err := msg.getString("method")
		if err != nil {
			return fmt.Errorf("wsNotification unmarshalling failure: %v", err)
		}
		if method == subscriptionMethod {
			notifParams := &wsNotificationParams{}
			if err := msg.getForRef("params", notifParams); err != nil {
				return fmt.Errorf("wsNotification unmarshalling failure: %v", err)
			}

			c.sMtx.RLock()
			defer c.sMtx.RUnlock()

			var aggrErr aggregatedErr

		nextItem:
			for _, i := range notifParams.Items {
				subIDStr := strconv.FormatInt(i.SubID, 10)
				var publishFunc func(subID string, content interface{}) error
				switch i.Content.(type) {
				case *ubiquity.BlockIdentifier:
					publishFunc = func(subID string, content interface{}) error {
						if s, _ := c.blockIDSubscribers[subID]; s != nil {
							s <- content.(*ubiquity.BlockIdentifier)
							return nil
						}
						return fmt.Errorf("no '%s' subscriber for subID #%s", blockIDChannel, subIDStr)
					}
				case *ubiquity.Block:
					publishFunc = func(subID string, content interface{}) error {
						if s, _ := c.blockSubscribers[subID]; s != nil {
							s <- content.(*ubiquity.Block)
							return nil
						}
						return fmt.Errorf("no '%s' subscriber for subID #%s", blockChannel, subIDStr)
					}
				case *ubiquity.Tx:
					publishFunc = func(subID string, content interface{}) error {
						if s, _ := c.txSubscribers[subID]; s != nil {
							s <- content.(*ubiquity.Tx)
							return nil
						}
						return fmt.Errorf("no '%s' subscriber for subID #%s", txChannel, subIDStr)
					}
				default:
					aggrErr = append(aggrErr, fmt.Errorf("unsupported type of content for subID #%s: %T",
						subIDStr, i.Content))
					continue nextItem
				}

				publishC := make(chan error)
				go func() {
					publishC <- publishFunc(subIDStr, i.Content)
				}()

				select {
				case publishErr := <-publishC:
					if publishErr != nil {
						aggrErr = append(aggrErr, fmt.Errorf("failed to publish an item for subID #%s: %v",
							subIDStr, publishErr))
					}
				case <-time.After(time.Duration(c.publishTimeoutSec) * time.Second):
					aggrErr = append(aggrErr, fmt.Errorf("incomming message for subID #%s will be dropped: "+
						"timed out while waiting for it to be consumed", subIDStr))
				}
			}

			if len(aggrErr) > 0 {
				return fmt.Errorf("failed while processing the notification items: %v", aggrErr)
			}
			return nil
		} else {
			return fmt.Errorf("unsupported wsNotification method: %s", method)
		}
	default:
		return errors.New("unsupported JSON structure")
	}
}

// SubscribeBlockIDs subscribes a channel "ubiquity.block_identifiers".
// It returns a notification channel with subID or error.
func (c *Client) SubscribeBlockIDs() (string, <-chan *ubiquity.BlockIdentifier, error) {
	subID, err := c.subscribe(blockIDChannel, make(map[string]interface{}))
	if err != nil {
		return "", nil, fmt.Errorf("failed to subscribe '%s': %v", blockIDChannel, err)
	}

	subC := make(chan *ubiquity.BlockIdentifier)

	c.sMtx.Lock()
	c.blockIDSubscribers[subID] = subC
	c.sMtx.Unlock()

	return subID, subC, nil
}

// UnsubscribeBlockIDs unsubscribes a channel "ubiquity.block_identifiers".
func (c *Client) UnsubscribeBlockIDs(subID string) error {
	c.sMtx.Lock()
	s := c.blockIDSubscribers[subID]
	if s != nil {
		delete(c.blockIDSubscribers, subID)
		close(s)
	} else {
		c.logger.warn("Couldn't find '%s' subscriber for subID #%s", subID, blockIDChannel)
	}
	c.sMtx.Unlock()

	if err := c.unsubscribe(subID, blockIDChannel); err != nil {
		return fmt.Errorf("failed to unsubscribe '%s': %v", blockIDChannel, err)
	}

	return nil
}

// SubscribeBlocks subscribes a channel "ubiquity.blocks".
// It returns a notification channel with subID or error.
func (c *Client) SubscribeBlocks() (string, <-chan *ubiquity.Block, error) {
	subID, err := c.subscribe(blockChannel, make(map[string]interface{}))
	if err != nil {
		return "", nil, fmt.Errorf("failed to subscribe '%s': %v", blockChannel, err)
	}

	subC := make(chan *ubiquity.Block)

	c.sMtx.Lock()
	c.blockSubscribers[subID] = subC
	c.sMtx.Unlock()

	return subID, subC, nil
}

// UnsubscribeBlocks unsubscribes a channel "ubiquity.blocks".
func (c *Client) UnsubscribeBlocks(subID string) error {
	c.sMtx.Lock()
	s := c.blockSubscribers[subID]
	if s != nil {
		delete(c.blockSubscribers, subID)
		close(s)
	} else {
		c.logger.warn("Couldn't find '%s' subscriber for subID #%s", subID, blockChannel)
	}
	c.sMtx.Unlock()

	if err := c.unsubscribe(subID, blockChannel); err != nil {
		return fmt.Errorf("failed to unsubscribe '%s': %v", blockChannel, err)
	}

	return nil
}

type TxsFilter struct {
	Assets    []string
	Addresses []string
}

// SubscribeTxs subscribes a channel "ubiquity.txs" with optionally provided filtering over assets and addresses.
// It returns a notification channel with subID or error.
func (c *Client) SubscribeTxs(filter *TxsFilter) (string, <-chan *ubiquity.Tx, error) {
	if filter == nil {
		filter = &TxsFilter{}
	}

	subID, err := c.subscribe(txChannel, map[string]interface{}{
		"addresses": filter.Addresses,
		"assets":    filter.Assets,
	})
	if err != nil {
		return "", nil, fmt.Errorf("failed to subscribe '%s': %v", txChannel, err)
	}

	subC := make(chan *ubiquity.Tx)

	c.sMtx.Lock()
	c.txSubscribers[subID] = subC
	c.sMtx.Unlock()

	return subID, subC, nil
}

// UnsubscribeTxs unsubscribes a channel "ubiquity.txs"
func (c *Client) UnsubscribeTxs(subID string) error {
	c.sMtx.Lock()
	s := c.txSubscribers[subID]
	if s != nil {
		delete(c.txSubscribers, subID)
		close(s)
	} else {
		c.logger.warn("Couldn't find '%s' subscriber for subID #%s", subID, txChannel)
	}
	c.sMtx.Unlock()

	if err := c.unsubscribe(subID, txChannel); err != nil {
		return fmt.Errorf("failed to unsubscribe '%s': %v", txChannel, err)
	}

	return nil
}

func (c *Client) subscribe(channel string, details map[string]interface{}) (string, error) {
	reqID := c.nextRequestID()
	resp, err := c.sendWSRequest(newSubscribeRequest(reqID, channel, details))
	if err != nil || len(resp.Error) > 0 {
		return "", fmt.Errorf("failed to subscribe: err = %v and response = %v", err, resp)
	}

	subIDInF64, ok := resp.Result["subID"].(float64)
	if !ok {
		return "", fmt.Errorf("failed to convert 'subID' into float64")
	}
	subID := int64(subIDInF64)
	subIDStr := strconv.FormatInt(subID, 10)
	return subIDStr, nil
}

func (c *Client) unsubscribe(subID, channel string) error {
	reqID := c.nextRequestID()
	subIDNum, err := strconv.ParseInt(subID, 10, 64)
	if err != nil {
		return fmt.Errorf("failed to convert subID %s: %v", subID, err)
	}
	resp, err := c.sendWSRequest(newUnsubscribeRequest(reqID, channel, subIDNum))
	if err != nil || len(resp.Error) > 0 {
		return fmt.Errorf("failed to unsubscribe: err = %v and response = %v", err, resp)
	}

	removed, ok := resp.Result["removed"].(bool)
	if !ok {
		return fmt.Errorf("failed to convert 'removed' into bool")
	}
	if !removed {
		c.logger.warn("Couldn't remove subscription with ID '%s'", subID)
	}
	return nil
}

func (c *Client) nextRequestID() int64 {
	c.rMtx.Lock()
	defer c.rMtx.Unlock()

	c.reqIDCounter++
	return c.reqIDCounter
}

func (c *Client) sendWSRequest(req *wsRequest) (*wsResponse, error) {
	c.rMtx.Lock()
	defer c.rMtx.Unlock()

	err := c.wsConn.WriteJSON(req)
	if err != nil {
		return nil, fmt.Errorf("websocket request failure: %v", err)
	}

	respC := make(chan *wsResponse)
	reqIDStr := strconv.FormatInt(req.ID, 10)
	c.pendingRequests.Store(reqIDStr, respC)
	defer c.pendingRequests.Delete(reqIDStr)
	select {
	case resp := <-respC:
		if resp != nil {
			return resp, nil
		} else {
			return nil, errors.New("received nil response for req. #" + reqIDStr)
		}
	case <-time.After(time.Duration(c.responseTimeoutSec) * time.Second):
		return nil, errors.New("timed out while waiting for request #" + reqIDStr + " response")
	}
}

// Close closes WS client and releases the resources
func (c *Client) Close() error {
	c.unsubscribeAll()
	err := c.wsConn.Close()
	if err != nil {
		return fmt.Errorf("failed to close WS conn: %v", err)
	}
	return nil
}

func (c *Client) unsubscribeAll() {
	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		defer wg.Done()
		for subID, _ := range c.blockIDSubscribers {
			if err := c.UnsubscribeBlockIDs(subID); err != nil {
				c.logger.error("failed to unsubscribe: %v", err)
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for subID, _ := range c.blockSubscribers {
			if err := c.UnsubscribeBlocks(subID); err != nil {
				c.logger.error("failed to unsubscribe: %v", err)
			}
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for subID, _ := range c.txSubscribers {
			if err := c.UnsubscribeTxs(subID); err != nil {
				c.logger.error("failed to unsubscribe: %v", err)
			}
		}
	}()

	wg.Wait()
}
